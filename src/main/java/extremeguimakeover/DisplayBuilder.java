package extremeguimakeover;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Dimension2D;
import javafx.util.Duration;

import java.util.HashMap;


public class DisplayBuilder<B extends DisplayBuilder<B>> {
    private HashMap<String, Property> properties = new HashMap<>();


    // ******************** Constructors **************************************
    protected DisplayBuilder() {}


    // ******************** Methods *******************************************
    public static final DisplayBuilder create() {
        return new DisplayBuilder();
    }

    public DisplayBuilder pushed(final boolean PUSHED) {
        properties.put("pushed", new SimpleBooleanProperty(PUSHED));
        return this;
    }

    public DisplayBuilder title(final String title) {
        properties.put("title", new SimpleStringProperty(title));
        return this;
    }

    public DisplayBuilder track(final int track) {
        properties.put("track", new SimpleIntegerProperty(track));
        return this;
    }

    public DisplayBuilder folder(final int folder) {
        properties.put("folder", new SimpleIntegerProperty(folder));
        return this;
    }

    public DisplayBuilder remaining(final Duration remaining) {
        properties.put("remaining", new SimpleObjectProperty<>(remaining));
        return this;
    }

    public DisplayBuilder pitch(final double pitch) {
        properties.put("pitch", new SimpleDoubleProperty(pitch));
        return this;
    }

    public DisplayBuilder autoBpm(final boolean autoBpm) {
        properties.put("autoBpm", new SimpleBooleanProperty(autoBpm));
        return this;
    }

    public DisplayBuilder bpm(final double bpm) {
        properties.put("bpm", new SimpleDoubleProperty(bpm));
        return this;
    }

    public DisplayBuilder autoCue(final boolean autoCue) {
        properties.put("autoCue", new SimpleBooleanProperty(autoCue));
        return this;
    }

    public DisplayBuilder cue(final boolean cue) {
        properties.put("cue", new SimpleBooleanProperty(cue));
        return this;
    }

    public DisplayBuilder bargraphValue(final double bargraphValue) {
        properties.put("bargraphValue", new SimpleDoubleProperty(bargraphValue));
        return this;
    }


    public final B minSize(final double WIDTH, final double HEIGHT) {
        properties.put("minSize", new SimpleObjectProperty<>(new Dimension2D(WIDTH, HEIGHT)));
        return (B)this;
    }
    public final B prefSize(final double WIDTH, final double HEIGHT) {
            properties.put("prefSize", new SimpleObjectProperty<>(new Dimension2D(WIDTH, HEIGHT)));
            return (B)this;
        }
    public final B maxSize(final double WIDTH, final double HEIGHT) {
        properties.put("maxSize", new SimpleObjectProperty<>(new Dimension2D(WIDTH, HEIGHT)));
        return (B)this;
    }

    public final B minWidth(final double MIN_WIDTH) {
        properties.put("minWidth", new SimpleDoubleProperty(MIN_WIDTH));
        return (B)this;
    }
    public final B minHeight(final double MIN_HEIGHT) {
        properties.put("minHeight", new SimpleDoubleProperty(MIN_HEIGHT));
        return (B)this;
    }

    public final B prefWidth(final double PREF_WIDTH) {
        properties.put("prefWidth", new SimpleDoubleProperty(PREF_WIDTH));
        return (B)this;
    }
    public final B prefHeight(final double PREF_HEIGHT) {
        properties.put("prefHeight", new SimpleDoubleProperty(PREF_HEIGHT));
        return (B)this;
    }

    public final B maxWidth(final double MAX_WIDTH) {
        properties.put("maxWidth", new SimpleDoubleProperty(MAX_WIDTH));
        return (B)this;
    }
    public final B maxHeight(final double MAX_HEIGHT) {
        properties.put("maxHeight", new SimpleDoubleProperty(MAX_HEIGHT));
        return (B)this;
    }

    public final B layoutX(final double LAYOUT_X) {
        properties.put("layoutX", new SimpleDoubleProperty(LAYOUT_X));
        return (B)this;
    }
    public final B layoutY(final double LAYOUT_Y) {
        properties.put("layoutY", new SimpleDoubleProperty(LAYOUT_Y));
        return (B)this;
    }

    public final B translateX(final double TRANSLATE_X) {
        properties.put("translateX", new SimpleDoubleProperty(TRANSLATE_X));
        return (B)this;
    }
    public final B translateY(final double TRANSLATE_Y) {
        properties.put("translateY", new SimpleDoubleProperty(TRANSLATE_Y));
        return (B)this;
    }

    public final B scaleX(final double SCALE_X) {
        properties.put("scaleX", new SimpleDoubleProperty(SCALE_X));
        return (B)this;
    }
    public final B scaleY(final double SCALE_Y) {
        properties.put("scaleY", new SimpleDoubleProperty(SCALE_Y));
        return (B)this;
    }


    public final Display build() {
        final Display CONTROL = new Display();
        properties.forEach((key, property) -> {
            if ("minSize".equals(key)) {
                Dimension2D dim = ((ObjectProperty<Dimension2D>) property).get();
                CONTROL.setMinSize(dim.getWidth(), dim.getHeight());
            } else if ("prefSize".equals(key)) {
                Dimension2D dim = ((ObjectProperty<Dimension2D>) property).get();
                CONTROL.setPrefSize(dim.getWidth(), dim.getHeight());
            } else if ("maxSize".equals(key)) {
                Dimension2D dim = ((ObjectProperty<Dimension2D>) property).get();
                CONTROL.setMaxSize(dim.getWidth(), dim.getHeight());
            } else if("minWidth".equals(key)) {
                CONTROL.setMinWidth(((DoubleProperty) property).get());
            } else if("minHeight".equals(key)) {
                CONTROL.setMinHeight(((DoubleProperty) property).get());
            } else if("prefWidth".equals(key)) {
                CONTROL.setPrefWidth(((DoubleProperty) property).get());
            } else if("prefHeight".equals(key)) {
                CONTROL.setPrefHeight(((DoubleProperty) property).get());
            } else if("maxWidth".equals(key)) {
                CONTROL.setMaxWidth(((DoubleProperty) property).get());
            } else if("maxHeight".equals(key)) {
                CONTROL.setMaxHeight(((DoubleProperty) property).get());
            } else if ("layoutX".equals(key)) {
                CONTROL.setLayoutX(((DoubleProperty) property).get());
            } else if ("layoutY".equals(key)) {
                CONTROL.setLayoutY(((DoubleProperty) property).get());
            } else if ("translateX".equals(key)) {
                CONTROL.setTranslateX(((DoubleProperty) property).get());
            } else if ("translateY".equals(key)) {
                CONTROL.setTranslateY(((DoubleProperty) property).get());
            } else if ("scaleX".equals(key)) {
                CONTROL.setScaleX(((DoubleProperty) property).get());
            } else if ("scaleY".equals(key)) {
                CONTROL.setScaleY(((DoubleProperty) property).get());
            } else if ("pushed".equals(key)) {
                CONTROL.setPushed(((BooleanProperty) property).get());
            } else if ("title".equals(key)) {
                CONTROL.setTitle(((StringProperty) property).get());
            } else if ("track".equals(key)) {
                CONTROL.setTrack(((IntegerProperty) property).get());
            } else if ("folder".equals(key)) {
                CONTROL.setFolder(((IntegerProperty) property).get());
            } else if ("remaining".equals(key)) {
                CONTROL.setRemaining(((ObjectProperty<Duration>) property).get());
            } else if ("pitch".equals(key)) {
                CONTROL.setPitch(((DoubleProperty) property).get());
            } else if ("autoBpm".equals(key)) {
                CONTROL.setAutoBpm(((BooleanProperty) property).get());
            } else if ("bpm".equals(key)) {
                CONTROL.setBpm(((DoubleProperty) property).get());
            } else if ("autoCue".equals(key)) {
                CONTROL.setAutoCue(((BooleanProperty) property).get());
            } else if ("cue".equals(key)) {
                CONTROL.setCue(((BooleanProperty) property).get());
            } else if ("bargraphValue".equals(key)) {
                CONTROL.setBarGraphValue(((DoubleProperty) property).get());
            }

        });

        return CONTROL;
    }
}

