package extremeguimakeover;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.DoublePropertyBase;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.css.PseudoClass;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.util.Duration;


public class Display extends Control {
    private static final PseudoClass PUSHED_PSEUDO_CLASS = PseudoClass.getPseudoClass("pushed");
    private BooleanProperty          pushed;

    private StringProperty           title;
    private IntegerProperty          track;
    private IntegerProperty          folder;
    private ObjectProperty<Duration> remaining;
    private DoubleProperty           pitch;
    private BooleanProperty          autoBpm;
    private DoubleProperty           bpm;
    private BooleanProperty          autoCue;
    private BooleanProperty          cue;
    private DoubleProperty           barGraphValue;
    private BooleanProperty          on;


    // ******************** Constructors **************************************
    public Display() {
        getStyleClass().add("display");
    }


    // ******************** Methods *******************************************
    public final boolean isOn() {
        return null == on ? false : on.get();
    }
    public final void setOn(final boolean ON) {
        onProperty().set(ON);
    }
    public final BooleanProperty onProperty() {
        if (null == on) {
            on = new SimpleBooleanProperty(this, "on", false);
            on = new BooleanPropertyBase(false) {
                @Override public void set(final boolean ON) {
                    super.set(ON);
                    if (!ON) setPushed(false);
                }
                @Override public Object getBean() { return Display.this; }
                @Override public String getName() { return "on"; }
            };
        }
        return on;
    }

    public final boolean isPushed() {
        return null == pushed ? false : pushed.get();
    }
    public final void setPushed(final boolean pushed) {
        pushedProperty().set(pushed);
        if (pushed) {
            fireBackEvent(new Display.BackEvent(this, null, Display.BackEvent.BACK_SELECTED));
        } else {
            fireBackEvent(new Display.BackEvent(this, null, Display.BackEvent.BACK_DESELECTED));
        }
    }
    public final BooleanProperty pushedProperty() {
        if (null == pushed) {
            pushed = new BooleanPropertyBase() {
                @Override protected void invalidated() { pseudoClassStateChanged(PUSHED_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "pushed"; }
            };
        }
        return pushed;
    }

    public String getTitle() {
        return null == title ? "---" : title.get();
    }
    public void setTitle(final String title) {
        titleProperty().set(title);
    }
    public StringProperty titleProperty() {
        if (null == title) {
            title = new SimpleStringProperty(this, "title", "---");
        }
        return title;
    }

    public int getTrack() {
        return null == track ? 0 : track.get();
    }
    public void setTrack(final int track) {
        trackProperty().set(track);
    }
    public IntegerProperty trackProperty() {
        if (null == track) {
            track = new SimpleIntegerProperty(this, "track", 0);
        }
        return track;
    }

    public int getFolder() {
        return null == folder ? 0 : folder.get();
    }
    public void setFolder(final int folder) {
        folderProperty().set(folder);
    }
    public IntegerProperty folderProperty() {
        if (null == folder) {
            folder = new SimpleIntegerProperty(this, "folder", 0);
        }
        return folder;
    }

    public Duration getRemaining() {
        return null == remaining ? Duration.ZERO : remaining.get();
    }
    public void setRemaining(final Duration remaining) {
        remainingProperty().set(remaining);
    }
    public ObjectProperty<Duration> remainingProperty() {
        if (null == remaining) {
            remaining = new SimpleObjectProperty<>(this, "remaining", Duration.ZERO);
        }
        return remaining;
    }

    public double getPitch() {
        return null == pitch ? 0d : pitch.get();
    }
    public void setPitch(final double pitch) {
        pitchProperty().set(pitch);
    }
    public DoubleProperty pitchProperty() {
        if (null == pitch) {
            pitch = new SimpleDoubleProperty(this, "pitch", 0d);
        }
        return pitch;
    }

    public boolean isAutoBpm() {
        return null == autoBpm ? false : autoBpm.get();
    }
    public void setAutoBpm(final boolean autoBpm) {
        autoBpmProperty().set(autoBpm);
    }
    public BooleanProperty autoBpmProperty() {
        if (null == autoBpm) {
            autoBpm = new SimpleBooleanProperty(this, "autoBpm", false);
        }
        return autoBpm;
    }

    public double getBpm() {
        return null == bpm ? 0d : bpm.get();
    }
    public void setBpm(final double bpm) {
        bpmProperty().set(bpm);
    }
    public DoubleProperty bpmProperty() {
        if (null == bpm) {
            bpm = new DoublePropertyBase(90d) {
                @Override public void set(final double BPM) {
                    super.set(clamp(90d, 180d, BPM));
                }
                @Override public Object getBean() {
                    return Display.this;
                }
                @Override public String getName() {
                    return "bpm";
                }
            };
        }
        return bpm;
    }

    public boolean isAutoCue() {
        return null == autoCue ? false : autoCue.get();
    }
    public void setAutoCue(final boolean autoCue) {
        autoCueProperty().set(autoCue);
    }
    public BooleanProperty autoCueProperty() {
        if (null == autoCue) {
            autoCue = new SimpleBooleanProperty(this, "autoCue", false);
        }
        return autoCue;
    }

    public boolean isCue() {
        return null == cue ? false : cue.get();
    }
    public void setCue(final boolean cue) {
        cueProperty().set(cue);
    }
    public BooleanProperty cueProperty() {
        if (null == cue) {
            cue = new SimpleBooleanProperty(this, "cue", false);
        }
        return cue;
    }

    public double getBarGraphValue() {
        return null == barGraphValue ? 0 : barGraphValue.get();
    }
    public void setBarGraphValue(final double value) {        
        barGraphValueProperty().set(value);
    }
    public DoubleProperty barGraphValueProperty() {
        if (null == barGraphValue) {            
            barGraphValue = new DoublePropertyBase(0) {
                @Override public void set(final double VALUE) {
                    super.set(clamp(0d, 1d, VALUE));
                }
                @Override public Object getBean() {
                    return Display.this;
                }
                @Override public String getName() {
                    return "barGraphValue";
                }
            };
        }
        return barGraphValue;
    }

    private double clamp(final double min, final double max, final double value) {
        if (value < min) {
            return min;
        }
        if (value > max) {
            return max;
        }
        return value;
    }
    

    // ******************** Style related *************************************
    @Override protected Skin createDefaultSkin() {
        return new DisplaySkin(this);
    }

    @Override protected String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }


    // ******************** Event Handling ************************************
    public final ObjectProperty<EventHandler<BackEvent>> onBackSelectedProperty() { return onBackSelected; }
    public final void setOnBackSelected(EventHandler<BackEvent> value) { onBackSelectedProperty().set(value); }
    public final EventHandler<BackEvent> getOnBackSelected() { return onBackSelectedProperty().get(); }
    private ObjectProperty<EventHandler<BackEvent>> onBackSelected = new ObjectPropertyBase<EventHandler<BackEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onBackSelected";}
    };

    public final ObjectProperty<EventHandler<BackEvent>> onBackDeselectedProperty() { return onBackDeselected; }
    public final void setOnBackDeselected(EventHandler<BackEvent> value) { onBackDeselectedProperty().set(value); }
    public final EventHandler<BackEvent> getOnBackDeselected() { return onBackDeselectedProperty().get(); }
    private ObjectProperty<EventHandler<BackEvent>> onBackDeselected = new ObjectPropertyBase<EventHandler<BackEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onBackDeselected";}
    };

    public void fireBackEvent(final BackEvent EVENT) {
        fireEvent(EVENT);
        final EventType TYPE = EVENT.getEventType();
        final EventHandler<BackEvent> HANDLER;
        if (BackEvent.BACK_SELECTED == TYPE) {
            HANDLER = getOnBackSelected();
        } else if (BackEvent.BACK_DESELECTED == TYPE) {
            HANDLER = getOnBackDeselected();
        } else {
            HANDLER = null;
        }

        if (HANDLER != null) {
            HANDLER.handle(EVENT);
        }
    }


    // ******************** Inner Classes *************************************
    public static class BackEvent extends Event {
        public static final EventType<BackEvent> BACK_SELECTED   = new EventType(ANY, "backSelected");
        public static final EventType<BackEvent> BACK_DESELECTED = new EventType(ANY, "backDeselected");


        // ******************* Constructors ***************************************
        public BackEvent(final Object SOURCE, final EventTarget TARGET, final EventType<BackEvent> EVENT_TYPE) {
            super(SOURCE, TARGET, EVENT_TYPE);        
        }        
    }
}

