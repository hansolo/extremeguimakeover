package extremeguimakeover;/**
 * Created by
 * User: hansolo
 * Date: 13.09.13
 * Time: 14:26
 */

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.css.PseudoClass;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.transform.Transform;


public class DirectionButton extends Region {
    private static final double      PREFERRED_WIDTH  = 67;
    private static final double      PREFERRED_HEIGHT = 67;
    private static final double      MINIMUM_WIDTH    = 67;
    private static final double      MINIMUM_HEIGHT   = 67;
    private static final double      MAXIMUM_WIDTH    = 1024;
    private static final double      MAXIMUM_HEIGHT   = 1024;

    private static final PseudoClass TOP_PSEUDO_CLASS    = PseudoClass.getPseudoClass("top");
    private static final PseudoClass RIGHT_PSEUDO_CLASS  = PseudoClass.getPseudoClass("right");
    private static final PseudoClass BOTTOM_PSEUDO_CLASS = PseudoClass.getPseudoClass("bottom");
    private static final PseudoClass LEFT_PSEUDO_CLASS   = PseudoClass.getPseudoClass("left");

    double                           size;
    private Region                   topArrow;
    private Region                   rightArrow;
    private Region                   bottomArrow;
    private Region                   leftArrow;
    private Region                   background;
    private Region                   foreground;
    private Region                   bars;

    // CSS pseudo classes
    private BooleanProperty          top;
    private BooleanProperty          right;
    private BooleanProperty          bottom;
    private BooleanProperty          left;


    // ******************** Constructors **************************************
    public DirectionButton() {
        getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        getStyleClass().add("direction-button");

        init();
        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void init() {
        if (Double.compare(getPrefWidth(), 0.0) <= 0 || Double.compare(getPrefHeight(), 0.0) <= 0 ||
            Double.compare(getWidth(), 0.0) <= 0 || Double.compare(getHeight(), 0.0) <= 0) {
            if (getPrefWidth() > 0 && getPrefHeight() > 0) {
                setPrefSize(getPrefWidth(), getPrefHeight());
            } else {
                setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
            }
        }

        if (Double.compare(getMinWidth(), 0.0) <= 0 || Double.compare(getMinHeight(), 0.0) <= 0) {
            setMinSize(MINIMUM_WIDTH, MINIMUM_HEIGHT);
        }

        if (Double.compare(getMaxWidth(), 0.0) <= 0 || Double.compare(getMaxHeight(), 0.0) <= 0) {
            setMaxSize(MAXIMUM_WIDTH, MAXIMUM_HEIGHT);
        }
    }

    private void initGraphics() {
        topArrow = new Region();
        topArrow.getStyleClass().add("top-arrow");

        rightArrow = new Region();
        rightArrow.getStyleClass().add("right-arrow");

        bottomArrow = new Region();
        bottomArrow.getStyleClass().add("bottom-arrow");

        leftArrow = new Region();
        leftArrow.getStyleClass().add("left-arrow");

        background = new Region();
        background.getStyleClass().add("background");

        foreground = new Region();
        foreground.getStyleClass().add("foreground");
        foreground.setOnMousePressed(event -> handleClickEvent(event));
        foreground.setOnMouseReleased(event -> handleClickEvent(event));

        bars = new Region();
        bars.getStyleClass().add("bars");
        bars.setMouseTransparent(true);

        getChildren().addAll(topArrow, rightArrow, bottomArrow, leftArrow, background, foreground, bars);
        resize();
    }

    private void registerListeners() {
        widthProperty().addListener(observable -> resize());
        heightProperty().addListener(observable -> resize());
    }


    // ******************** Methods *******************************************
    private void handleControlPropertyChanged(final String PROPERTY) {
        if ("RESIZE".equals(PROPERTY)) {
            resize();
        }
    }

    private void handleClickEvent(MouseEvent event) {
        double    x     = event.getX();
        double    y     = event.getY();
        double    fSize = 0.6119402985 * size;
        EventType type  = event.getEventType();

        if(MouseEvent.MOUSE_PRESSED == type) {
            if (x > 0 && x < fSize * 0.25 && y > fSize * 0.25 && y < fSize * 0.75) {
                setTop(false);
                setRight(false);
                setBottom(false);
                setLeft(true);
                foreground.getTransforms().clear();
                foreground.getTransforms().add(Transform.translate(-2, 0));
                bars.getTransforms().clear();
                bars.getTransforms().add(Transform.translate(-2, 0));
                fireDirectionEvent(new DirectionEvent(this, this, DirectionEvent.LEFT));
            } else if (x > fSize * 0.75 && x < fSize && y > fSize * 0.25 && y < fSize * 0.75) {
                setTop(false);
                setRight(true);
                setBottom(false);
                setLeft(false);
                foreground.getTransforms().clear();
                foreground.getTransforms().add(Transform.translate(2, 0));
                bars.getTransforms().clear();
                bars.getTransforms().add(Transform.translate(2, 0));
                fireDirectionEvent(new DirectionEvent(this, this, DirectionEvent.RIGHT));
            } else if (x > fSize * 0.25 && x < fSize * 0.75 && y > 0 && y < fSize * 0.25) {
                setTop(true);
                setRight(false);
                setBottom(false);
                setLeft(false);
                foreground.getTransforms().clear();
                foreground.getTransforms().add(Transform.translate(0, -2));
                bars.getTransforms().clear();
                bars.getTransforms().add(Transform.translate(0, -2));
                fireDirectionEvent(new DirectionEvent(this, this, DirectionEvent.TOP));
            } else if (x > fSize * 0.25 && x < fSize * 0.75 && y > fSize * 0.75 && y < fSize) {
                setTop(false);
                setRight(false);
                setBottom(true);
                setLeft(false);
                foreground.getTransforms().clear();
                foreground.getTransforms().add(Transform.translate(0, 2));
                bars.getTransforms().clear();
                bars.getTransforms().add(Transform.translate(0, 2));
                fireDirectionEvent(new DirectionEvent(this, this, DirectionEvent.BOTTOM));
            }
        } else if (MouseEvent.MOUSE_RELEASED == type) {
            setTop(false);
            setRight(false);
            setBottom(false);
            setLeft(false);
            foreground.getTransforms().clear();
            bars.getTransforms().clear();
        }
    }

    @Override protected double computePrefWidth(final double PREF_HEIGHT) {
        double prefHeight = PREFERRED_HEIGHT;
        if (PREF_HEIGHT != -1) {
            prefHeight = Math.max(0, PREF_HEIGHT - getInsets().getTop() - getInsets().getBottom());
        }
        return super.computePrefWidth(prefHeight);
    }
    @Override protected double computePrefHeight(final double PREF_WIDTH) {
        double prefWidth = PREFERRED_WIDTH;
        if (PREF_WIDTH != -1) {
            prefWidth = Math.max(0, PREF_WIDTH - getInsets().getLeft() - getInsets().getRight());
        }
        return super.computePrefWidth(prefWidth);
    }

    @Override protected double computeMinWidth(final double MIN_HEIGHT) {
        return super.computeMinWidth(Math.max(MINIMUM_HEIGHT, MIN_HEIGHT - getInsets().getTop() - getInsets().getBottom()));
    }
    @Override protected double computeMinHeight(final double MIN_WIDTH) {
        return super.computeMinHeight(Math.max(MINIMUM_WIDTH, MIN_WIDTH - getInsets().getLeft() - getInsets().getRight()));
    }

    @Override protected double computeMaxWidth(final double MAX_HEIGHT) {
        return super.computeMaxWidth(Math.min(MAXIMUM_HEIGHT, MAX_HEIGHT - getInsets().getTop() - getInsets().getBottom()));
    }
    @Override protected double computeMaxHeight(final double MAX_WIDTH) {
        return super.computeMaxHeight(Math.min(MAXIMUM_WIDTH, MAX_WIDTH - getInsets().getLeft() - getInsets().getRight()));
    }

    public final boolean isTop() {
        return null == top ? false : top.get();
    }
    public final void setTop(final boolean TOP) {
        topProperty().set(TOP);
    }
    public final BooleanProperty topProperty() {
        if (null == top) {
            top = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(TOP_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "top"; }
            };
        }
        return top;
    }

    public final boolean isRight() {
        return null == right ? false : right.get();
    }
    public final void setRight(final boolean RIGHT) {
        rightProperty().set(RIGHT);
    }
    public final BooleanProperty rightProperty() {
        if (null == right) {
            right = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(RIGHT_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "right"; }
            };
        }
        return right;
    }

    public final boolean isBottom() {
        return null == bottom ? false : bottom.get();
    }
    public final void setBottom(final boolean BOTTOM) {
        bottomProperty().set(BOTTOM);
    }
    public final BooleanProperty bottomProperty() {
        if (null == bottom) {
            bottom = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(BOTTOM_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "bottom"; }
            };
        }
        return bottom;
    }

    public final boolean isLeft() {
        return null == left ? false : left.get();
    }
    public final void setLeft(final boolean LEFT) {
        leftProperty().set(LEFT);
    }
    public final BooleanProperty leftProperty() {
        if (null == left) {
            left = new BooleanPropertyBase(false) {
                @Override protected void invalidated() { pseudoClassStateChanged(LEFT_PSEUDO_CLASS, get()); }
                @Override public Object getBean() { return this; }
                @Override public String getName() { return "left"; }
            };
        }
        return left;
    }


    // ******************** Resizing ******************************************
    private void resize() {
        if (getWidth() > 0 && getHeight() > 0) {
            size = getWidth() < getHeight() ? getWidth() : getHeight();
            setPrefSize(size, size);

            topArrow.setPrefSize(0.1492537313 * size, 0.0746268657 * size);
            topArrow.relocate(0.4253731344 * size, 0);

            rightArrow.setPrefSize(0.0746268657 * size, 0.1492537313 * size);
            rightArrow.relocate(0.9253731343 * size, 0.4253731344 * size);

            bottomArrow.setPrefSize(0.1492537313 * size, 0.0746268657 * size);
            bottomArrow.relocate(0.4253731344 * size, 0.9253731343 * size);

            leftArrow.setPrefSize(0.0746268657 * size, 0.1492537313 * size);
            leftArrow.relocate(0, 0.4253731344 * size);

            background.setPrefSize(0.7611940299 * size, 0.7611940299 * size);
            background.relocate(0.119402985 * size, 0.119402985 * size);

            foreground.setPrefSize(0.6119402985 * size, 0.6119402985 * size);
            foreground.relocate(0.1940298508 * size, 0.1940298508 * size);

            bars.setPrefSize(0.4328358209 * size, 0.4328358209 * size);
            bars.relocate(0.2835820896 * size, 0.2835820896 * size);
        }
    }


    // ******************** Event Handling ************************************
    public final ObjectProperty<EventHandler<DirectionEvent>> onDirectionTopProperty() { return onDirectionTop; }
    public final void setOnDirectionTop(EventHandler<DirectionEvent> value) { onDirectionTopProperty().set(value); }
    public final EventHandler<DirectionEvent> getOnDirectionTop() { return onDirectionTopProperty().get(); }
    private ObjectProperty<EventHandler<DirectionEvent>> onDirectionTop = new ObjectPropertyBase<EventHandler<DirectionEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onDirectionTop";}
    };

    public final ObjectProperty<EventHandler<DirectionEvent>> onDirectionRightProperty() { return onDirectionRight; }
    public final void setOnDirectionRight(EventHandler<DirectionEvent> value) { onDirectionRightProperty().set(value); }
    public final EventHandler<DirectionEvent> getOnDirectionRight() { return onDirectionRightProperty().get(); }
    private ObjectProperty<EventHandler<DirectionEvent>> onDirectionRight = new ObjectPropertyBase<EventHandler<DirectionEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onDirectionRight";}
    };

    public final ObjectProperty<EventHandler<DirectionEvent>> onDirectionBottomProperty() { return onDirectionBottom; }
    public final void setOnDirectionBottom(EventHandler<DirectionEvent> value) { onDirectionBottomProperty().set(value); }
    public final EventHandler<DirectionEvent> getOnDirectionBottom() { return onDirectionBottomProperty().get(); }
    private ObjectProperty<EventHandler<DirectionEvent>> onDirectionBottom = new ObjectPropertyBase<EventHandler<DirectionEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onDirectionBottom";}
    };

    public final ObjectProperty<EventHandler<DirectionEvent>> onDirectionLeftProperty() { return onDirectionLeft; }
    public final void setOnDirectionLeft(EventHandler<DirectionEvent> value) { onDirectionLeftProperty().set(value); }
    public final EventHandler<DirectionEvent> getOnDirectionLeft() { return onDirectionLeftProperty().get(); }
    private ObjectProperty<EventHandler<DirectionEvent>> onDirectionLeft = new ObjectPropertyBase<EventHandler<DirectionEvent>>() {
        @Override public Object getBean() { return this; }
        @Override public String getName() { return "onDirectionLeft";}
    };
    
    public void fireDirectionEvent(final DirectionEvent EVENT) {
        fireEvent(EVENT);
        final EventType             TYPE = EVENT.getEventType();
        final EventHandler<DirectionEvent> HANDLER;
        if (DirectionEvent.TOP == TYPE) {
            HANDLER = getOnDirectionTop();
        } else if (DirectionEvent.RIGHT == TYPE) {
            HANDLER = getOnDirectionRight();
        } else if (DirectionEvent.BOTTOM == TYPE) {
            HANDLER = getOnDirectionBottom();
        } else if (DirectionEvent.LEFT == TYPE) {
            HANDLER = getOnDirectionLeft();    
        } else {
            HANDLER = null;
        }

        if (HANDLER != null) {
            HANDLER.handle(EVENT);
        }
    }

    
    // ******************** Inner Classes *************************************
    public static class DirectionEvent extends Event {
        public static final EventType<DirectionEvent> TOP    = new EventType(ANY, "top");
        public static final EventType<DirectionEvent> RIGHT  = new EventType(ANY, "right");
        public static final EventType<DirectionEvent> BOTTOM = new EventType(ANY, "bottom");
        public static final EventType<DirectionEvent> LEFT   = new EventType(ANY, "left");

        // ******************** Constructors **************************************
        public DirectionEvent(final Object SOURCE, final EventTarget TARGET, EventType<DirectionEvent> TYPE) {
            super(SOURCE, TARGET, TYPE);
        }
    }
}