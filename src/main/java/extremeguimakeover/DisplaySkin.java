package extremeguimakeover;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class DisplaySkin extends SkinBase<Display> implements Skin<Display> {

    private static final double            MINIMUM_WIDTH    = 578;
    private static final double            MINIMUM_HEIGHT   = 232;
    private static final double            MAXIMUM_WIDTH    = 578;
    private static final double            MAXIMUM_HEIGHT   = 232;
    private static final double            PREFERRED_WIDTH  = 578;
    private static final double            PREFERRED_HEIGHT = 232;    

    private double   aspectRatio;
    private double   width;
    private double   height;
    private Pane     pane;

    private Text     titleText;
    private Text     text;
    private Label    folderText;
    private Text     folder;
    private Label    trackText;
    private Text     track;
    private Text     remain;
    private Label    remainingMinutes;
    private Label    remainingCollon;
    private Label    remainingSeconds;
    private Label    remainingMillis;
    private Label    pitchText;
    private Text     pitch;
    private Region   autoCueBack;
    private Text     autoCueText;
    private Text     cue;
    private Region   pauseIcon;
    private SVGPath  bargraph;
    private Label    bpmText;
    private Text     autoBpm;
    private Text     bpm;
    private Region   highlight;
    private Region   backArrow;
    private Text     backText;
    private Region   backButton;
    private Region   backButtonMain;
    private Font     smallFont;
    private Font     mediumFont;
    private Font     bigFont;
    private Timeline timeline;

    
    // ******************** Constructors **************************************
    public DisplaySkin(final Display CONTROL) {
        super(CONTROL);
        aspectRatio = PREFERRED_HEIGHT / PREFERRED_WIDTH;
        timeline    = new Timeline();
        init();
        initGraphics();
        registerListeners();
    }

    
    // ******************** Initialization ************************************
    private void init() {
        if (Double.compare(getSkinnable().getPrefWidth(), 0.0) <= 0 || Double.compare(getSkinnable().getPrefHeight(),
                                                                                      0.0) <= 0
            || Double.compare(getSkinnable().getWidth(), 0.0) <= 0 || Double.compare(getSkinnable().getHeight(),
                                                                                     0.0) <= 0) {
            if (getSkinnable().getPrefWidth() > 0 && getSkinnable().getPrefHeight() > 0) {
                getSkinnable().setPrefSize(getSkinnable().getPrefWidth(), getSkinnable().getPrefHeight());
            } else {
                getSkinnable().setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
            }
        }

        if (Double.compare(getSkinnable().getMinWidth(), 0.0) <= 0 || Double.compare(getSkinnable().getMinHeight(), 0.0) <= 0) {
            getSkinnable().setMinSize(MINIMUM_WIDTH, MINIMUM_HEIGHT);
        }

        if (Double.compare(getSkinnable().getMaxWidth(), 0.0) <= 0 || Double.compare(getSkinnable().getMaxHeight(), 0.0) <= 0) {
            getSkinnable().setMaxSize(MAXIMUM_WIDTH, MAXIMUM_HEIGHT);
        }

        if (getSkinnable().getPrefWidth() != PREFERRED_WIDTH || getSkinnable().getPrefHeight() != PREFERRED_HEIGHT) {
            aspectRatio = getSkinnable().getPrefHeight() / getSkinnable().getPrefWidth();
        }
    }

    private void initGraphics() {
        smallFont = Font.loadFont(getClass().getResourceAsStream("/extremeguimakeover/bebas.ttf"), (0.0431034483 * PREFERRED_HEIGHT));
        mediumFont = Font.loadFont(getClass().getResourceAsStream("/extremeguimakeover/bebas.ttf"), (0.1336206897 * PREFERRED_HEIGHT));
        bigFont = Font.loadFont(getClass().getResourceAsStream("/extremeguimakeover/bebas.ttf"), (0.1982758621 * PREFERRED_HEIGHT));

        titleText = new Text(getSkinnable().getTitle());
        titleText.getStyleClass().setAll("big-text");
        titleText.setOpacity(0);

        text = new Text("0");
        text.getStyleClass().setAll("medium-text");
        text.setOpacity(0);

        folderText = new Label(String.format("%02d", getSkinnable().getFolder()));
        folderText.getStyleClass().setAll("medium-text");
        folderText.setAlignment(Pos.CENTER_RIGHT);
        folderText.setOpacity(0);

        folder = new Text("folder");
        folder.getStyleClass().setAll("small-text");

        trackText = new Label(String.format("%02d", getSkinnable().getTrack()));
        trackText.getStyleClass().setAll("big-text");
        trackText.setAlignment(Pos.CENTER_RIGHT);
        trackText.setOpacity(0);

        track = new Text("track");
        track.getStyleClass().setAll("small-text");
                                
        remainingMinutes = new Label("00");
        remainingMinutes.getStyleClass().setAll("medium-text");
        remainingMinutes.setAlignment(Pos.CENTER);
        remainingMinutes.setOpacity(0);

        remainingCollon = new Label(":");
        remainingCollon.getStyleClass().setAll("medium-text");
        remainingCollon.setAlignment(Pos.CENTER);
        remainingCollon.setOpacity(0);
        
        remainingSeconds = new Label("00");
        remainingSeconds.getStyleClass().setAll("medium-text");
        remainingSeconds.setAlignment(Pos.CENTER);
        remainingSeconds.setOpacity(0);
            
        remainingMillis = new Label("00");
        remainingMillis.getStyleClass().setAll("medium-text");
        remainingMillis.setAlignment(Pos.CENTER);
        remainingMillis.setOpacity(0);

        remain = new Text("remain");
        remain.getStyleClass().setAll("small-text");

        pitchText = new Label(String.format(Locale.US, "%.1f", getSkinnable().getPitch()));
        pitchText.getStyleClass().setAll("medium-text");
        pitchText.setAlignment(Pos.CENTER_RIGHT);
        pitchText.setOpacity(0);

        pitch = new Text("pitch");
        pitch.getStyleClass().setAll("small-text");

        autoCueBack = new Region();
        autoCueBack.getStyleClass().setAll("autocue-back");
        autoCueBack.setVisible(getSkinnable().isAutoCue());

        autoCueText = new Text("auto    cue");
        autoCueText.getStyleClass().setAll("small-text");
        autoCueText.setVisible(getSkinnable().isAutoCue());

        cue = new Text("cue");
        cue.getStyleClass().setAll("small-text-red");
        cue.setVisible(getSkinnable().isCue());

        pauseIcon = new Region();
        pauseIcon.getStyleClass().setAll("pauseicon");
        pauseIcon.setVisible(getSkinnable().isCue());

        bargraph = new SVGPath();
        bargraph.setContent("M 265.0 146.0 L 265.0 148.0 L 273.0 148.0 L 273.0 146.0 L 265.0 146.0 ZM 255.0 146.0 L 255.0 148.0 L 263.0 148.0 L 263.0 146.0 L 255.0 146.0 ZM 245.0 146.0 L 245.0 148.0 L 253.0 148.0 L 253.0 146.0 L 245.0 146.0 ZM 235.0 146.0 L 235.0 148.0 L 243.0 148.0 L 243.0 146.0 L 235.0 146.0 ZM 225.0 146.0 L 225.0 148.0 L 233.0 148.0 L 233.0 146.0 L 225.0 146.0 ZM 215.0 146.0 L 215.0 148.0 L 223.0 148.0 L 223.0 146.0 L 215.0 146.0 ZM 205.0 146.0 L 205.0 148.0 L 213.0 148.0 L 213.0 146.0 L 205.0 146.0 ZM 195.0 146.0 L 195.0 148.0 L 203.0 148.0 L 203.0 146.0 L 195.0 146.0 ZM 185.0 146.0 L 185.0 148.0 L 193.0 148.0 L 193.0 146.0 L 185.0 146.0 ZM 175.0 146.0 L 175.0 148.0 L 183.0 148.0 L 183.0 146.0 L 175.0 146.0 ZM 165.0 146.0 L 165.0 148.0 L 173.0 148.0 L 173.0 146.0 L 165.0 146.0 Z");
        bargraph.setFill(Color.TRANSPARENT);
        bargraph.setOpacity(0);

        bpm = new Text("bpm");
        bpm.getStyleClass().setAll("small-text");

        autoBpm = new Text("auto");
        autoBpm.getStyleClass().setAll("small-text-red");

        bpmText = new Label(String.format(Locale.US, "%.1f", getSkinnable().getBpm()));
        bpmText.getStyleClass().setAll("medium-text");
        bpmText.setAlignment(Pos.CENTER_RIGHT);
        bpmText.setOpacity(0);

        backArrow = new Region();
        backArrow.getStyleClass().setAll("back-arrow");

        backText = new Text("back");
        backText.getStyleClass().setAll("small-text");

        backButton = new Region();
        backButton.getStyleClass().setAll("back-button");

        backButtonMain = new Region();
        backButtonMain.getStyleClass().setAll("back-button-main");

        highlight = new Region();
        highlight.getStyleClass().add("highlight");

        pane = new Pane();
        pane.getChildren().setAll(titleText,
                                  text,
                                  folder,
                                  folderText,
                                  track,
                                  trackText,
                                  remain,
                                  remainingMinutes,
                                  remainingCollon,
                                  remainingSeconds,
                                  remainingMillis,
                                  pitch,
                                  pitchText,
                                  autoCueBack,
                                  autoCueText,
                                  cue,
                                  pauseIcon,
                                  bargraph,
                                  bpm,
                                  backButtonMain,
                                  autoBpm,
                                  bpmText,
                                  backArrow,
                                  backText,
                                  backButton,
                                  highlight);

        getChildren().setAll(pane);
    }

    private void registerListeners() {
        getSkinnable().widthProperty().addListener(observable -> handleControlPropertyChanged("RESIZE"));
        getSkinnable().heightProperty().addListener(observable -> handleControlPropertyChanged("RESIZE"));
        getSkinnable().titleProperty().addListener(observable -> handleControlPropertyChanged("TITLE"));
        getSkinnable().trackProperty().addListener(observable -> handleControlPropertyChanged("TRACK"));
        getSkinnable().folderProperty().addListener(observable -> handleControlPropertyChanged("FOLDER"));
        getSkinnable().remainingProperty().addListener(observable -> handleControlPropertyChanged("REMAINING"));
        getSkinnable().pitchProperty().addListener(observable -> handleControlPropertyChanged("PITCH"));
        getSkinnable().autoBpmProperty().addListener(observable -> handleControlPropertyChanged("AUTO_BPM"));
        getSkinnable().bpmProperty().addListener(observable -> handleControlPropertyChanged("BPM"));
        getSkinnable().autoCueProperty().addListener(observable -> handleControlPropertyChanged("AUTO_CUE"));
        getSkinnable().cueProperty().addListener(observable -> handleControlPropertyChanged("CUE"));
        getSkinnable().barGraphValueProperty().addListener(observable -> handleControlPropertyChanged("BARGRAPH_VALUE"));
        getSkinnable().pushedProperty().addListener(observable -> handleControlPropertyChanged("PUSHED"));
        getSkinnable().onProperty().addListener(observable -> handleControlPropertyChanged("ON"));
        backButton.setOnMousePressed(this::handleMouseClick);
    }

    
    // ******************** Methods *******************************************
    protected void handleControlPropertyChanged(final String PROPERTY) {
        if ("RESIZE".equals(PROPERTY)) {
            resize();
        } else if ("TITLE".equals(PROPERTY)) {
            titleText.setText(getSkinnable().getTitle());
        } else if ("TRACK".equals(PROPERTY)) {
            trackText.setText(String.format("%02d", getSkinnable().getTrack()));
        } else if ("FOLDER".equals(PROPERTY)) {
            folderText.setText(String.format("%02d", getSkinnable().getFolder()));
        } else if ("REMAINING".equals(PROPERTY)) {
            long remaining = (long) getSkinnable().getRemaining().toMillis();                                                
            remainingMinutes.setText(String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(remaining)));
            remainingSeconds.setText(String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(remaining) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remaining))));
            remainingMillis.setText((String.format("%02d", TimeUnit.MILLISECONDS.toMillis(remaining) - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(remaining)))).substring(0, 2));            
        } else if ("PITCH".equals(PROPERTY)) {
            pitchText.setText(String.format(Locale.US, "%.1f", getSkinnable().getPitch()));
        } else if ("AUTO_BPM".equals(PROPERTY)) {
            autoBpm.setVisible(getSkinnable().isAutoBpm());
        } else if ("BPM".equals(PROPERTY)) {
            bpmText.setText(String.format(Locale.US, "%.1f", getSkinnable().getBpm()));
        } else if ("AUTO_CUE".equals(PROPERTY)) {
            autoCueText.setVisible(getSkinnable().isAutoCue());
            autoCueBack.setVisible(getSkinnable().isAutoCue());
        } else if ("CUE".equals(PROPERTY)) {
            cue.setVisible(getSkinnable().isCue());
            pauseIcon.setVisible(getSkinnable().isCue());
        } else if ("BARGRAPH_VALUE".equals(PROPERTY)) {
            double barGraphValue = createDiscrete(getSkinnable().getBarGraphValue());
            bargraph.setFill(barGraphValue < 1 ? new LinearGradient(bargraph.getLayoutBounds().getMinX(), 0,
                    bargraph.getLayoutBounds().getMaxX(), 0,
                    false, CycleMethod.NO_CYCLE,
                    new Stop(0.0, Color.rgb(245, 86, 72)),
                    new Stop(barGraphValue, Color.rgb(245, 86, 72)),
                    new Stop(barGraphValue, Color.TRANSPARENT),
                    new Stop(1.0, Color.TRANSPARENT))
                    : Color.rgb(245, 86, 72));
        } else if ("PUSHED".equals(PROPERTY)) {

        } else if ("ON".equals(PROPERTY)) {
            if (getSkinnable().isOn()) {
                fadeIn();
            } else {
                fadeOut();
            }            
        }
    }

    protected void handleMouseClick(MouseEvent event) {
        if (getSkinnable().isOn()) {
            getSkinnable().setPushed(!getSkinnable().isPushed());
        }
    }

    private void fadeIn() {        
        ParallelTransition parallelFadeIn = new ParallelTransition(createFadeInTransition(folderText),
                                                                   createFadeInTransition(trackText),
                                                                   createFadeInTransition(pitchText),
                                                                   createFadeInTransition(titleText),
                                                                   createFadeInTransition(bpmText),
                                                                   createFadeInTransition(remainingMinutes),
                                                                   createFadeInTransition(remainingCollon),
                                                                   createFadeInTransition(remainingSeconds),
                                                                   createFadeInTransition(remainingMillis),
                                                                   createFadeInTransition(text),
                                                                   createFadeInTransition(bargraph));
        parallelFadeIn.setOnFinished(event -> initBarGraph());
        parallelFadeIn.play();        
    }
    
    private void fadeOut() {
        ParallelTransition parallelFadeOut = new ParallelTransition(createFadeOutTransition(folderText),
                                                                    createFadeOutTransition(trackText),
                                                                    createFadeOutTransition(pitchText),
                                                                    createFadeOutTransition(titleText),
                                                                    createFadeOutTransition(bpmText),
                                                                    createFadeOutTransition(remainingMinutes),
                                                                    createFadeOutTransition(remainingCollon),
                                                                    createFadeOutTransition(remainingSeconds),
                                                                    createFadeOutTransition(remainingMillis),
                                                                    createFadeOutTransition(text),
                                                                    createFadeOutTransition(bargraph));
        parallelFadeOut.play();    
    }
    
    private FadeTransition createFadeInTransition(final Node NODE) {
        FadeTransition fadeIn = new FadeTransition(Duration.millis(500), NODE);
        fadeIn.setFromValue(0);
        fadeIn.setToValue(1);
        fadeIn.setInterpolator(Interpolator.EASE_IN);
        return fadeIn;
    }

    private FadeTransition createFadeOutTransition(final Node NODE) {
        FadeTransition fadeOut = new FadeTransition(Duration.millis(700), NODE);
        fadeOut.setFromValue(1);
        fadeOut.setToValue(0);
        fadeOut.setInterpolator(Interpolator.EASE_IN);
        return fadeOut;
    }
    
    private void initBarGraph() {        
        KeyValue kvLow1 = new KeyValue(getSkinnable().barGraphValueProperty(), 0, Interpolator.EASE_IN);
        KeyValue kvHigh = new KeyValue(getSkinnable().barGraphValueProperty(), 1);
        KeyValue kvLow2 = new KeyValue(getSkinnable().barGraphValueProperty(), 0, Interpolator.EASE_OUT);

        KeyFrame kf1 = new KeyFrame(Duration.ZERO, kvLow1);
        KeyFrame kf2 = new KeyFrame(Duration.millis(100), kvHigh);
        KeyFrame kf3 = new KeyFrame(Duration.millis(1500), kvLow2);
        
        timeline.getKeyFrames().setAll(kf1, kf2, kf3);
        timeline.play();
    }
    
    private double createDiscrete(final double value) {
        double discreteValue = clamp(0d, 1d, value);
        double step = 0.09;
        discreteValue = ((int) (discreteValue / step)) * 10 / bargraph.getLayoutBounds().getWidth();
        getSkinnable().setPitch(discreteValue);
        return discreteValue;
    }

    private double clamp(final double min, final double max, final double value) {
        if (value < min) {
            return min;
        }
        if (value > max) {
            return max;
        }
        return value;
    }

    
    // ******************** Resizing ******************************************
    private void resize() {
        width = getSkinnable().getWidth();
        height = getSkinnable().getHeight();

        if (aspectRatio * width > height) {
            width = 1 / (aspectRatio / height);
        } else if (1 / (aspectRatio / height) > width) {
            height = aspectRatio * width;
        }

        if (width > 0 && height > 0) {
            smallFont = Font.font("Bebas", FontWeight.BLACK, FontPosture.REGULAR, 0.0431034483 * height);
            mediumFont = Font.font("Bebas", FontWeight.LIGHT, FontPosture.REGULAR, 0.1336206897 * height);
            bigFont = Font.font("Bebas", FontWeight.EXTRA_LIGHT, FontPosture.REGULAR, 0.1982758621 * height);

            titleText.relocate(0.068 * width, 0.425 * height);
            titleText.setFont(bigFont);

            text.relocate(0.527681660899654 * width, 0.735 * height);
            text.setFont(mediumFont);

            folderText.setPrefSize(0.0553633218 * width, 0.1681034483 * height);
            folderText.relocate(0.07 * width, 0.63 * height);
            folderText.setFont(mediumFont);

            folder.relocate(0.073 * width, 0.805 * height);
            folder.setFont(smallFont);

            trackText.setPrefSize(0.0899653979 * width, 0.2370689655 * height);
            trackText.relocate(0.155 * width, 0.56 * height);
            trackText.setFont(bigFont);

            track.relocate(0.19204152249134948 * width, 0.805 * height);
            track.setFont(smallFont);
                        
            remainingMinutes.setPrefSize(0.1 * width, 0.1681034483 * height);
            remainingMinutes.relocate(0.261 * width, 0.63 * height);
            remainingMinutes.setFont(mediumFont);

            remainingCollon.setPrefSize(0.1 * width, 0.1681034483 * height);
            remainingCollon.relocate(0.299 * width, 0.63 * height);
            remainingCollon.setFont(mediumFont);

            remainingSeconds.setPrefSize(0.1 * width, 0.1681034483 * height);
            remainingSeconds.relocate(0.335 * width, 0.63 * height);
            remainingSeconds.setFont(mediumFont);

            remainingMillis.setPrefSize(0.1 * width, 0.1681034483 * height);
            remainingMillis.relocate(0.400 * width, 0.63 * height);
            remainingMillis.setFont(mediumFont);
            
            remain.relocate(0.425 * width, 0.805 * height);
            remain.setFont(smallFont);

            pitchText.setPrefSize(0.0761245675 * width, 0.1681034483 * height);
            pitchText.relocate(0.5711 * width, 0.63 * height);
            pitchText.setFont(mediumFont);

            pitch.relocate(0.605 * width, 0.805 * height);
            pitch.setFont(smallFont);

            autoCueBack.setPrefSize(0.076 * width, 0.061 * height);
            autoCueBack.relocate(0.07093425605536333 * width, 0.1625 * height);

            autoCueText.relocate(0.074 * width, 0.158 * height);
            autoCueText.setFont(smallFont);

            cue.relocate(0.21453287197231835 * width, 0.158 * height);
            cue.setFont(smallFont);

            pauseIcon.setPrefSize(0.010380622837370242 * width, 0.034482758620689655 * height);
            pauseIcon.relocate(0.19550173010380623 * width, 0.175 * height);

            //bargraph.setPrefSize(0.18685121107266436 * width, 0.008620689655172414 * height);
            bargraph.relocate(0.28546712802768165 * width, 0.63 * height);

            bpmText.setPrefSize(0.1314878893 * width, 0.1681034483 * height);
            bpmText.relocate(0.5155 * width, 0.315 * height);
            bpmText.setFont(mediumFont);

            bpm.relocate(0.615916955017301 * width, 0.5 * height);
            bpm.setFont(smallFont);

            autoBpm.relocate(0.5726643598615917 * width, 0.5 * height);
            autoBpm.setFont(smallFont);

            backButtonMain.setPrefSize(0.08824 * width, 0.09914 * height);
            backButtonMain.relocate(0.80104 * width, 0.23276 * height);
            
            backArrow.setPrefSize(0.02941176470588 * width, 0.04310344827586 * height);
            backArrow.relocate(0.80795847750865 * width, 0.125 * height);

            backText.relocate(0.84083044982699 * width, 0.12068965517241 * height);
            backText.setFont(smallFont);

            backButton.setPrefSize(0.11245674740484 * width, 0.16379310344828 * height);
            backButton.relocate(0.78892733564014 * width, 0.19827586206897 * height);

            highlight.setPrefSize(0.41868512110727 * width, height);
        }
    }
}
