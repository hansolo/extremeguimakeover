/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extremeguimakeover;

import javafx.scene.control.SkinBase;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 *
 * @author antonepple
 */
public class JogSkin extends SkinBase<JogControl> {

    private double dragOffset;
    private ImageView jogBackground;
    private StackPane jog;
    private ImageView jogDents;

    public JogSkin(JogControl c) {
        super(c);
        initialize();
    }

    private void initialize() {
        jog = new StackPane();
        jogBackground = new ImageView(new Image("extremeguimakeover/jogcenter.png"));
        jogBackground.getStyleClass().add("jog-body");
        jog.getChildren().addAll(jogBackground);

        
        jogDents = new ImageView("extremeguimakeover/jog-dents-1.png");
        jogDents.getStyleClass().add("jog-dents");
        getChildren().setAll(jog, jogDents);
        
        getSkinnable().setOnMousePressed(me -> {
            double dragStart = calculateAngle(me.getX(), me.getY());
            double value = getSkinnable().getValue();
            dragOffset = value - dragStart;
            getSkinnable().switchJogMode(true);
        });
        getSkinnable().setOnMouseReleased(me -> {
            getSkinnable().setValue(calculateAngle(me.getX(), me.getY()) + dragOffset);
            getSkinnable().switchJogMode(false);
        });
        getSkinnable().setOnMouseDragged(me -> getSkinnable().setValue(calculateAngle(me.getX(), me.getY()) + dragOffset));
        getSkinnable().valueProperty().addListener((ov, t, t1) -> rotateJogger(t1.doubleValue()*2%360));
    }

    private double calculateAngle(double mouseX, double mouseY) {
        double cx = getSkinnable().getWidth() / 2;
        double cy = getSkinnable().getHeight() / 2;
        double mouseAngle   = Math.toDegrees(Math.atan((mouseY - cy) / (mouseX - cx)));
        double topZeroAngle = 0;
        topZeroAngle = mouseX < cx ? -(90 - mouseAngle) : (90 + mouseAngle);        
        return topZeroAngle;
    }

    private void rotateJogger(double angle) {
        jogBackground.setRotate(angle);         
        jogDents.setRotate(angle%15);
    }

}
