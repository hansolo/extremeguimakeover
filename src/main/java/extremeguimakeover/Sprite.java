package extremeguimakeover;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.util.Duration;


/**
 * Created by
 * User: hansolo
 * Date: 09.09.13
 * Time: 11:30
 */
public class Sprite extends Region {
    private ImageView       sprite;
    private IntegerProperty index;
    private Timeline        timeline;
    private boolean         running;
    private Image           imageStripe;
    private int             noOfFrames;
    private int             timeInMs;
    private int             frameWidth;
    private int             frameHeight;
    private double          halfFrameWidth;
    private double          halfFrameHeight;
    private double          x;
    private double          y;


    // ******************** Constructor ***************************************
    public Sprite(final Image IMAGE_STRIPE, final int NO_OF_FRAMES, final int TIME_IN_MS) {
        imageStripe     = IMAGE_STRIPE;
        noOfFrames      = NO_OF_FRAMES;
        timeInMs        = TIME_IN_MS;
        index           = new SimpleIntegerProperty(this, "index", 0);
        timeline        = new Timeline();
        running         = false;
        frameWidth      = (int) (IMAGE_STRIPE.getWidth() / NO_OF_FRAMES);
        frameHeight     = (int) IMAGE_STRIPE.getHeight();
        halfFrameWidth  = frameWidth * 0.5;
        halfFrameHeight = frameHeight * 0.5;
        x               = 0;
        y               = 0;
        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void initGraphics() {
        sprite = new ImageView(imageStripe);
        sprite.setViewport(new Rectangle2D(0, 0, frameWidth, frameHeight));
        getChildren().setAll(sprite);
    }

    private void registerListeners() {
        index.addListener(observable -> update());
    }


    // ******************** Methods ***************************************
    public void handleMouseEvent(final MouseEvent EVENT) {
        if (MouseEvent.MOUSE_MOVED == EVENT.getEventType()) {
            x = EVENT.getSceneX();
            y = EVENT.getSceneY();
            if (running) return;
            relocate();
        } else if (MouseEvent.MOUSE_CLICKED == EVENT.getEventType()) {
            play();
        }
    }

    public void playAnimationAt(final double X, final double Y) {
        if (running) return;
        x = X;
        y = Y;
        relocate();
        play();
    }

    private void relocate() {
        sprite.relocate(x - halfFrameWidth, y - halfFrameHeight);
    }

    private void play() {
        KeyValue kvStart = new KeyValue(index, 0);
        KeyValue kvStop  = new KeyValue(index, noOfFrames);
        KeyFrame kfStart = new KeyFrame(Duration.ZERO, kvStart);
        KeyFrame kfStop  = new KeyFrame(Duration.millis(timeInMs), kvStop);
        timeline.getKeyFrames().setAll(kfStart, kfStop);
        running = true;
        timeline.play();
        timeline.setOnFinished(event -> {
            running = false;
            sprite.relocate(x - halfFrameWidth, y - halfFrameHeight);
        });
    }

    private void update() {
        sprite.setViewport(new Rectangle2D(index.get() * frameWidth, 0, frameWidth, frameHeight));
    }
}
