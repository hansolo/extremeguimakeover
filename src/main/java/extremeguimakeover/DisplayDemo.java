package extremeguimakeover;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 * Created by
 * User: hansolo
 * Date: 19.09.13
 * Time: 14:18
 */
public class DisplayDemo extends Application{
    private int clicks;
    
    @Override public void start(Stage stage) throws Exception {
        Display display = DisplayBuilder.create()
                                        .title("JAX 2014")
                                        .folder(1)
                                        .track(5)
                                        .bpm(180.5)
                                        .autoBpm(true)
                                        .autoCue(true)
                                        .cue(true)
                                        .build();
        display.setOnMouseClicked(event -> {
            clicks = event.getClickCount();
            if (clicks == 2) {
                display.setOn(!display.isOn());
                clicks = 0;
            }
        });
        display.setOnBackSelected(event -> clicks = 0);
        display.setOnBackDeselected(event -> clicks = 0);
        
        StackPane pane = new StackPane(display);
        pane.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Scene scene = new Scene(pane);        
        
        stage.setScene(scene);
        stage.show();
        
                   
    }

    public static void main(String[] args) {
        launch(args);
    }
}
