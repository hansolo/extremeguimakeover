
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extremeguimakeover;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.SkinBase;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.SVGPath;

/**
 *
 * @author antonepple
 */
public class JoystickSkin extends SkinBase<JoystickControl> {

    private double dragOffset;
    private ImageView joystickKnob;
    private StackPane jog;
    private ImageView knobOverlay;
    private SVGPath clip;

    public JoystickSkin(JoystickControl c) {
        super(c);
        initialize();
    }

    private void initialize() {
        jog = new StackPane();

        joystickKnob = new ImageView(new Image("extremeguimakeover/background_joystick.png"));
        joystickKnob.getStyleClass().add("jog-body");
        
        knobOverlay = new ImageView("extremeguimakeover/knob_overlay.png");
        jog.getChildren().addAll( joystickKnob,knobOverlay);

        clip = new SVGPath();
        clip.setContent("M 23.1264 1.0814 C 22.366 2.8572 21.4696 4.1687 20.438 4.4451 C 19.4328 4.7144 18.0485 4.0694 16.5476 2.9651 C 14.5466 3.8366 12.6748 4.9491 10.9708 6.2687 C 11.1638 8.0839 11.024 9.5738 10.2989 10.2989 C 9.5737 11.0242 8.0844 11.164 6.2687 10.9708 C 4.9497 12.6748 3.8362 14.5466 2.9651 16.5476 C 4.0694 18.0485 4.7144 19.4328 4.4451 20.438 C 4.1687 21.4697 2.8573 22.366 1.0814 23.1264 C 0.8942 24.3904 0.7982 25.6839 0.7982 27 C 0.7982 27.803 0.8337 28.5983 0.9044 29.3829 C 2.77 30.1592 4.1595 31.08 4.4451 32.1457 C 4.744 33.2611 3.9173 34.8452 2.5863 36.5353 C 3.3071 38.3799 4.2309 40.1232 5.3304 41.7368 C 7.5642 41.3888 9.4521 41.4379 10.2989 42.2848 C 11.188 43.1739 11.1998 45.2098 10.7938 47.5897 C 12.2236 48.7166 13.7739 49.6977 15.4216 50.5108 C 17.3433 48.8873 19.1853 47.8029 20.438 48.1386 C 21.7423 48.4881 22.8297 50.4906 23.7 52.9965 C 24.7806 53.1323 25.8825 53.2018 27 53.2018 C 27.6165 53.2018 28.2283 53.18 28.8341 53.1381 C 29.7125 50.5659 30.8168 48.4947 32.1457 48.1386 C 33.5063 47.774 35.5634 49.0855 37.6613 50.9428 C 39.1774 50.2668 40.6157 49.4496 41.9634 48.5103 C 41.3952 45.7447 41.2828 43.2867 42.2848 42.2848 C 43.2865 41.283 45.7455 41.3955 48.5103 41.9634 C 49.4497 40.6159 50.2667 39.1771 50.9428 37.6613 C 49.0856 35.5634 47.774 33.5062 48.1386 32.1457 C 48.4947 30.8168 50.5658 29.7125 53.1381 28.8341 C 53.1799 28.2283 53.2018 27.6165 53.2018 27 C 53.2018 25.8825 53.1322 24.7806 52.9965 23.7 C 50.4906 22.8297 48.4881 21.7423 48.1386 20.438 C 47.8029 19.1852 48.8869 17.3436 50.5108 15.4216 C 49.6975 13.7733 48.7171 12.224 47.5897 10.7938 C 45.2106 11.1995 43.1736 11.1878 42.2848 10.2989 C 41.4382 9.4523 41.3891 7.567 41.7368 5.3339 C 40.1222 4.2337 38.3776 3.3078 36.5318 2.5863 C 34.8423 3.9164 33.2607 4.7439 32.1457 4.4451 C 31.08 4.1595 30.1592 2.77 29.3829 0.9044 C 28.5983 0.8337 27.8031 0.7982 27 0.7982 C 25.6839 0.7982 24.3904 0.8941 23.1264 1.0814 Z");
        joystickKnob.setClip(clip);
        getChildren().setAll(jog);

        getSkinnable().setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                double dragStart = calculateAngle(me.getX(), me.getY());
                double value = ((JoystickControl) getSkinnable()).getValue();
                dragOffset = value - dragStart;
            }
        });
        getSkinnable().setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                ((JoystickControl) getSkinnable()).setValue(calculateAngle(me.getX(), me.getY()) + dragOffset);

            }
        });
        getSkinnable().setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                ((JoystickControl) getSkinnable()).setValue(calculateAngle(me.getX(), me.getY()) + dragOffset);
            }
        });
        ((JoystickControl) getSkinnable()).valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                rotateJogger(t1.doubleValue());
            }
        });
    }

    private double calculateAngle(double mouseX, double mouseY) {
        double cx = getSkinnable().getWidth() / 2;
        double cy = getSkinnable().getHeight() / 2;
        double mouseAngle = Math.toDegrees(Math.atan((mouseY - cy) / (mouseX - cx)));
        double topZeroAngle = 0;
        if (mouseX < cx) {
            topZeroAngle = -(90 - mouseAngle);
        } else {
            topZeroAngle = (90 + mouseAngle);
        }
        return topZeroAngle;
    }

    private void rotateJogger(double angle) {
        clip.setRotate(angle);
    }

}
