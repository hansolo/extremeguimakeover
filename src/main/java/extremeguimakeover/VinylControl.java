/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package extremeguimakeover;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Control;

/**
 *
 * @author antonepple
 */
public class VinylControl extends Control {
    
    private final DoubleProperty value = new SimpleDoubleProperty();

    public double getValue() {
        return value.get();
    }
    
    public void setValue(double val) {
        value.set(val);
    }
    
    public DoubleProperty valueProperty() {
        return value;
    }
    
    @Override
    protected String getUserAgentStylesheet() {
        return "extremeguimakeover/vinyl.css"; //To change body of generated methods, choose Tools | Templates.
    }
    
}