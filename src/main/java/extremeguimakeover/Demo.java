/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package extremeguimakeover;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioSpectrumListener;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;


/**
 * Created by User: hansolo Date: 12.09.13 Time: 10:22
 */
public class Demo extends Application {
    private static final Media    CODE_HARD_SONG          = new Media(new File("src/main/resources/extremeguimakeover/CodeHard.mp3").toURI().toString());
    private static final Media    LADY_JAVA_SONG          = new Media(new File("src/main/resources/extremeguimakeover/LadyJava.mp3").toURI().toString());
    private static final Media    SWEET_HOME_SONG         = new Media(new File("src/main/resources/extremeguimakeover/SweetHomeJavaOne.mp3").toURI().toString());
    private static final int      MEDIA_PLAYER_CODE_HARD  = 0;
    private static final int      MEDIA_PLAYER_LADY_JAVA  = 1;
    private static final int      MEDIA_PLAYER_SWEET_HOME = 2;
    
    //Toni
    final private  Delta          dragDelta            = new Delta();
    private        Region         trackSearchRightIcon = new Region();
    private        Region         trackSearchLeftIcon  = new Region();
    private        Region         searchRightIcon      = new Region();
    private        Region         searchLeftIcon       = new Region();
    private        MediaPlayer[]  mediaPlayers         = new MediaPlayer[3];
    private static int            currentMediaPlayer   = MEDIA_PLAYER_CODE_HARD;
    private JogControl            jogControl;
    private JoystickControl       joystickControl;
    private VinylControl          vinylControl;
    private ChangeListenerImpl    jogModeListener;
    private ToggleButton          vinylButton;

    //Gerrit
    private ToggleButton          beatSelectButton;
    private Sprite                spriteExplosion;
    private InnerShadow           highlight;
    private InnerShadow           bottomShadow;
    private Region                regionPowerIcon;
    private ToggleButton          powerButton;
    private Led                   led;
    private DirectionButton       directionButton;
    private VBox                  sliderContainer;
    private Slider                tempoSlider;
    private Display               display;
    private Region                regionLoopInIcon;
    private ToggleButton          loopInButton;
    private Region                regionLoopOutIcon;
    private ToggleButton          loopOutButton;
    private Region                regionReLoopIcon;
    private ToggleButton          reLoopButton;
    private Button                resetTempoButton;
    private AudioSpectrumListener audioSpectrumListener;


    // ******************** Initialization ************************************
    @Override public void init() {
        // Toni
        trackSearchRightIcon.setId("track-search-right");
        trackSearchLeftIcon.setId("track-search-left");
        searchRightIcon.setId("search-right");
        searchLeftIcon.setId("search-left");
        jogModeListener = new ChangeListenerImpl();
        jogControl = createJogControl();
        joystickControl = createJoyStickControl();
        vinylControl = createVinylControl();
        vinylButton = createVinylButton();

        createMediaPlayers();
        switchJogMode(false);

        // Gerrit
        display = new Display();
        display.setTitle("Code Hard");
        display.setTrack(1);

        spriteExplosion = new Sprite(new Image(getClass().getResourceAsStream("explosion.png")), 72, 2000);
        highlight = new InnerShadow(BlurType.TWO_PASS_BOX, Color.WHITE, 1, 0.0, 0, 1);
        bottomShadow = new InnerShadow(BlurType.TWO_PASS_BOX, Color.BLACK, 1, 0.0, 0, -1);
        bottomShadow.setInput(highlight);

        regionPowerIcon = new Region();
        regionPowerIcon.setId("power-icon");
        regionPowerIcon.setPrefSize(22, 26);

        powerButton = createRoundToggleButton(61, regionPowerIcon, "power");
        powerButton.setOnAction(event -> {
            led.setOn(powerButton.isSelected());
            display.setOn(led.isOn());
            if (!led.isOn()) {
                spriteExplosion.playAnimationAt(led.getLayoutX() + led.getLayoutBounds().getWidth() * 0.5,
                                                led.getLayoutY() + led.getLayoutBounds().getHeight() * 0.5);}
        });        

        regionLoopInIcon = new Region();
        regionLoopInIcon.setId("loop-in-icon");
        regionLoopInIcon.setPrefSize(21, 22);

        loopInButton = createRoundToggleButton(45, regionLoopInIcon, "loop-in");

        regionLoopOutIcon = new Region();
        regionLoopOutIcon.setId("loop-out-icon");
        regionLoopOutIcon.setPrefSize(21, 22);

        loopOutButton = createRoundToggleButton(45, regionLoopOutIcon, "loop-out");

        regionReLoopIcon = new Region();
        regionReLoopIcon.setId("re-loop-icon");
        regionReLoopIcon.setPrefSize(21, 22);

        reLoopButton = createRoundToggleButton(45, regionReLoopIcon, "re-loop");

        led = new Led();

        directionButton = new DirectionButton();
        directionButton.setOnDirectionTop(event -> System.out.println("Top"));
        directionButton.setOnDirectionRight(event -> System.out.println("Right"));
        directionButton.setOnDirectionBottom(event -> System.out.println("Bottom"));
        directionButton.setOnDirectionLeft(event -> System.out.println("Left"));

        tempoSlider = new Slider();
        tempoSlider.setMin(0);
        tempoSlider.setMax(40);
        tempoSlider.setValue(17.825);
        tempoSlider.getStyleClass().add("tempo-slider");
        tempoSlider.valueProperty().addListener((ov, old, value) -> {
            if (value.doubleValue() < 1.69) {
                tempoSlider.setValue(1.69);
            } else if (value.doubleValue() > 37.34) {
                tempoSlider.setValue(37.34);
            }
            display.setBpm(90 + value.doubleValue() * 2.25);
            mediaPlayers[currentMediaPlayer].setRate(0.5 + value.doubleValue() / 40d);
        });
        tempoSlider.setMinSize(59, 347);
        tempoSlider.setPrefSize(59, 347);
        tempoSlider.setMaxSize(59, 347);

        Label tempoLabel = new Label("Tempo");
        tempoLabel.getStyleClass().add("tempo-label");
        tempoLabel.setPrefWidth(59);
        tempoLabel.setAlignment(Pos.CENTER);

        sliderContainer = new VBox();
        sliderContainer.setAlignment(Pos.CENTER);
        sliderContainer.setFillWidth(true);
        sliderContainer.setPrefSize(61, 395);
        sliderContainer.setSpacing(15);
        sliderContainer.getStyleClass().add("tempo-slider-container");
        sliderContainer.getChildren().addAll(tempoSlider, tempoLabel);
        
        resetTempoButton = createRoundButton(28, "", "smaller");
        resetTempoButton.setOnAction(event -> {
            tempoSlider.setValue(19.515);
            mediaPlayers[currentMediaPlayer].setRate(0.5 + 19.515 / 40d);            
        });        
        audioSpectrumListener = (timestamp, duration, magnitudes, phases) -> {
            float max = 0;
            for (float value : magnitudes) {
                max = Math.max(max, value + 60);
            }            
            display.setBarGraphValue(max / 20);            
        };                                                                       
        mediaPlayers[MEDIA_PLAYER_CODE_HARD].setAudioSpectrumListener(audioSpectrumListener);
        mediaPlayers[MEDIA_PLAYER_LADY_JAVA].setAudioSpectrumListener(audioSpectrumListener);
        mediaPlayers[MEDIA_PLAYER_SWEET_HOME].setAudioSpectrumListener(audioSpectrumListener);
    }


    // ******************** App start *****************************************
    @Override public void start(Stage stage) throws Exception {

        // ******************** Gerrit ****************************************        
        beatSelectButton = createSquareButton(51, 41, "     Beat\nselect", false);
        beatSelectButton.getStyleClass().add("beat");
        HBox effectButtons = new HBox();
        effectButtons.setSpacing(14);
        effectButtons.getChildren().addAll(createSquareButton(53, 48, "Skid", false),
                createSquareButton(53, 48, "Filter", false),
                createSquareButton(53, 48, "Phase", false),
                createSquareButton(53, 48, "Hold", false),
                createSquareButton(53, 48, "Echo", false),
                createSquareButton(53, 48, "Flanger", false),
                createSquareButton(53, 48, "Trans", false),
                createSquareButton(53, 48, "Pan", false));

        HBox padButtons = new HBox();
        padButtons.setAlignment(Pos.CENTER_RIGHT);
        padButtons.setSpacing(14);
        padButtons.getChildren().addAll(createSquareButton(53, 48, "1", true),
                createSquareButton(53, 48, "2", true),
                createSquareButton(53, 48, "3", true),
                createSquareButton(53, 48, "4", true));
        ToggleGroup padGroup = new ToggleGroup();
        for (Node node : padButtons.getChildren()) {
            ((ToggleButton) node).setToggleGroup(padGroup);
        }

        VBox taktButtons = new VBox();
        taktButtons.getStyleClass().add("vertical-button-box");
        taktButtons.setSpacing(17);
        taktButtons.setFillWidth(false);
        taktButtons.getChildren().addAll(createRoundToggleButton(40, "1"),
                createRoundToggleButton(40, "2"),
                createRoundToggleButton(40, "3"),
                createRoundToggleButton(40, "4"));
        ToggleGroup taktGroup = new ToggleGroup();
        for (Node node : taktButtons.getChildren()) {
            ((ToggleButton) node).setToggleGroup(taktGroup);
        }

        HBox minusPlusPane = new HBox();
        minusPlusPane.setSpacing(5);
        minusPlusPane.getChildren().addAll(createRoundButton(40, "-", "bigger"),
                createRoundButton(40, "+", "bigger"));

        Button cueButton = new Button();
        cueButton.setPrefSize(104, 104);
        cueButton.setMinSize(104, 104);
        cueButton.setId("cue");

        Button playPauseButton = new Button();        
        playPauseButton.setPrefSize(104, 104);
        playPauseButton.setMinSize(104, 104);
        playPauseButton.setId("play");
        playPauseButton.setOnAction(event -> {
            if (MediaPlayer.Status.PLAYING == mediaPlayers[currentMediaPlayer].getStatus()) {
                mediaPlayers[currentMediaPlayer].pause();
                display.setBarGraphValue(0);
            } else {
                mediaPlayers[currentMediaPlayer].play();
            }
        });

        // ******************** Toni ******************************************
        stage.initStyle(StageStyle.TRANSPARENT);
        VBox tracksearch = new VBox();
        tracksearch.getStyleClass().add("track-search-box");
        tracksearch.setLayoutX(10);
        tracksearch.getChildren().addAll(createTrackSearchButton(34, 34, trackSearchLeftIcon),
                                         createTrackSearchButton(34, 34, trackSearchRightIcon)
        );                
        trackSearchLeftIcon.setOnMousePressed(event -> {
            if (mediaPlayers[currentMediaPlayer].getCurrentTime().greaterThan(Duration.ZERO)) {
                mediaPlayers[currentMediaPlayer].seek(Duration.ZERO);
            } else {
                boolean isPlaying = mediaPlayers[currentMediaPlayer].getStatus() == MediaPlayer.Status.PLAYING;
                mediaPlayers[currentMediaPlayer].stop();
                jogControl.valueProperty().unbind();
                currentMediaPlayer--;
                if (currentMediaPlayer < 0) currentMediaPlayer = 2;
                switch(currentMediaPlayer) {
                    case 0: jogControl.valueProperty().bind(durationBindingCodeHard);  display.setTitle("Code Hard"); break;
                    case 1: jogControl.valueProperty().bind(durationBindingLadyJava);  display.setTitle("Lady Java"); break;
                    case 2: jogControl.valueProperty().bind(durationBindingSweetHome); display.setTitle("Sweet Home"); break;                    
                }                
                display.setTrack(currentMediaPlayer + 1);
                display.setRemaining(mediaPlayers[currentMediaPlayer].getMedia().getDuration());
                if (isPlaying) mediaPlayers[currentMediaPlayer].play();
            }
        });        
        trackSearchRightIcon.setOnMousePressed(event -> {
            boolean isPlaying = mediaPlayers[currentMediaPlayer].getStatus() == MediaPlayer.Status.PLAYING;
            mediaPlayers[currentMediaPlayer].stop();
            jogControl.valueProperty().unbind();            
            currentMediaPlayer++;
            if (currentMediaPlayer >= 3) currentMediaPlayer = 0;
            switch(currentMediaPlayer) {
                case 0: jogControl.valueProperty().bind(durationBindingCodeHard);  display.setTitle("Code Hard"); break;
                case 1: jogControl.valueProperty().bind(durationBindingLadyJava);  display.setTitle("Lady Java"); break;
                case 2: jogControl.valueProperty().bind(durationBindingSweetHome); display.setTitle("Sweet Home"); break;
            }                        
            display.setTrack(currentMediaPlayer + 1);
            display.setRemaining(mediaPlayers[currentMediaPlayer].getMedia().getDuration());
            if (isPlaying) mediaPlayers[currentMediaPlayer].play();            
        });

        VBox search = new VBox();
        search.setSpacing(20);
        search.getStyleClass().add("extreme-button");
        search.getStyleClass().add("track-search-box");

        // absolute positions
        search.setLayoutX(10);
        search.setLayoutY(570);

        tracksearch.setLayoutY(390);
        tracksearch.setSpacing(20);

        vinylButton.setLayoutX(626);
        vinylButton.setLayoutY(374);

        effectButtons.setLayoutX(84);
        effectButtons.setLayoutY(230);
        effectButtons.setScaleX(.9);
        effectButtons.setScaleY(.9);

        padButtons.setLayoutX(338);
        padButtons.setLayoutY(280);
        padButtons.setScaleX(0.9);
        padButtons.setScaleY(.9);

        taktButtons.setLayoutX(26);
        taktButtons.setLayoutY(10);
        taktButtons.setScaleX(.9);
        taktButtons.setScaleY(.9);

        powerButton.setLayoutX(628);
        powerButton.setLayoutY(19);
        powerButton.setScaleX(.92);
        powerButton.setScaleY(.92);

        led.setLayoutX(646);
        led.setLayoutY(347);
        led.setScaleX(.92);
        led.setScaleY(.92);

        directionButton.setLayoutX(518);
        directionButton.setLayoutY(360);
        directionButton.setScaleX(.9);
        directionButton.setScaleY(.9);

        sliderContainer.setLayoutX(626);
        sliderContainer.setLayoutY(466);
        sliderContainer.setScaleX(.92);
        sliderContainer.setScaleY(.92);

        minusPlusPane.setLayoutX(610);
        minusPlusPane.setLayoutY(870);
        minusPlusPane.setScaleX(.92);
        minusPlusPane.setScaleY(.92);

        display.setLayoutX(60);
        display.setLayoutY(-10);
        display.setScaleX(.9);
        display.setScaleY(.9);

        cueButton.setLayoutX(10);
        cueButton.setLayoutY(720);
        cueButton.setScaleX(.9);
        cueButton.setScaleY(.9);

        playPauseButton.setLayoutX(10);
        playPauseButton.setLayoutY(820);
        playPauseButton.setScaleX(.9);
        playPauseButton.setScaleY(.9);

        loopInButton.setLayoutX(114);
        loopInButton.setLayoutY(280);
        loopInButton.setScaleX(0.9);
        loopInButton.setScaleY(.9);

        loopOutButton.setLayoutX(190);
        loopOutButton.setLayoutY(280);
        loopOutButton.setScaleX(0.9);
        loopOutButton.setScaleY(.9);

        reLoopButton.setLayoutX(260);
        reLoopButton.setLayoutY(280);
        reLoopButton.setScaleX(0.9);
        reLoopButton.setScaleY(.9);
        
        resetTempoButton.setLayoutX(586);
        resetTempoButton.setLayoutY(774);
        resetTempoButton.setScaleX(.9);
        resetTempoButton.setScaleY(.9);
        
        beatSelectButton.setLayoutX(28);
        beatSelectButton.setLayoutY(266);
        beatSelectButton.setScaleX(.9);
        beatSelectButton.setScaleY(.9);

        search.getChildren().addAll(createTrackSearchButton(34, 34, searchLeftIcon),
                                    createTrackSearchButton(34, 34, searchRightIcon)
        );
        Pane pane = new Pane();                
        pane.getChildren().addAll(
                                         // 1. rectangular buttons                                
                effectButtons,
                padButtons,
                beatSelectButton,

                                         // 2. round buttons
                resetTempoButton,
                taktButtons,
                minusPlusPane,

                                         // 3. round buttons with icon
                loopInButton,
                loopOutButton,
                reLoopButton,
                powerButton,

                                         // 4. round metal
                search,
                tracksearch,

                                         // 5. round metal
                cueButton,
                playPauseButton,
                                             
                                         // 6. sliced metal Buttom
                vinylButton,

                                         // 7. led                
                led,

                display,                 // 8. display
                
                                         // 9. clipping
                joystickControl,
                vinylControl,

                                         // 10. Slider
                sliderContainer,
                                                                         
                                         // 11. JOG Control
                jogControl,
                                         // 12. Four sided button
                directionButton
        );

        StackPane overlayPane = new StackPane();
        overlayPane.getChildren().addAll(spriteExplosion);
        overlayPane.setMouseTransparent(true);
        overlayPane.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));

        StackPane bkgPane = new StackPane();
        bkgPane.getChildren().addAll(pane, overlayPane);

        pane.setOnMousePressed(mouseEvent -> {
            dragDelta.x = stage.getX() - mouseEvent.getScreenX();
            dragDelta.y = stage.getY() - mouseEvent.getScreenY();
        });
        pane.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() + dragDelta.x);
            stage.setY(mouseEvent.getScreenY() + dragDelta.y);
        });

        
        //final double scalingFactor = 0.9;
        //bkgPane.setScaleX(scalingFactor);
        //bkgPane.setScaleY(scalingFactor);
        
        Scene scene = new Scene(bkgPane, 700, 932);
        scene.setFill(Color.BLACK);
        scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        stage.setScene(scene);
        stage.show();

        display.setRemaining(CODE_HARD_SONG.getDuration());
    }
   
    public static void main(final String[] args) {
        Application.launch(args);
    }


    // ******************** Gerrit ********************************************
    private ToggleButton createSquareButton(double width, double height, String text, boolean number) {
        ToggleButton squareButton = new ToggleButton(text);
        squareButton.getStyleClass().addAll("extreme-button", "square", number ? "number" : "text");
        squareButton.setMinSize(width, height);
        squareButton.setPrefSize(width, height);
        squareButton.setMaxSize(width, height);
        squareButton.setAlignment(Pos.CENTER);
        return squareButton;
    }

    private ToggleButton createRoundToggleButton(double size, String text) {
        ToggleButton roundButton = new ToggleButton(text);
        roundButton.getStyleClass().addAll("extreme-button", "round");
        roundButton.setMinSize(size, size);
        roundButton.setPrefSize(size, size);
        roundButton.setMaxSize(size, size);
        return roundButton;
    }

    private ToggleButton createRoundToggleButton(double size, Region icon, String styleClass) {
        ToggleButton roundButton = new ToggleButton();
        roundButton.getStyleClass().addAll("extreme-button", "round", styleClass);
        roundButton.setMinSize(size, size);
        roundButton.setPrefSize(size, size);
        roundButton.setMaxSize(size, size);
        roundButton.setGraphic(icon);
        roundButton.setAlignment(Pos.CENTER);
        return roundButton;
    }

    private Button createRoundButton(double size, String text, String styleClass) {
        Button roundButton = new Button(text);
        roundButton.getStyleClass().addAll("extreme-button", "round", styleClass);
        roundButton.setMinSize(size, size);
        roundButton.setPrefSize(size, size);
        roundButton.setMaxSize(size, size);
        return roundButton;
    }

    
    // ******************** Toni ********************************************** 
    private Node createImage(String imagePath) {
        Image image = new Image(imagePath);
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(30);
        imageView.setFitWidth(30);
        return imageView;
    }

    private ToggleButton createVinylButton() {
        ToggleButton testMetal = new ToggleButton("  JavaFX  ");
        testMetal.getStyleClass().addAll("metal", "linear");
        return testMetal;
    }

    private JogControl createJogControl() {
        jogControl = new JogControl(this);
        jogControl.setId("jog");
        jogControl.getStyleClass().add("jog");
        jogControl.setLayoutX(122);
        jogControl.setLayoutY(368);

        return jogControl;
    }

    private DurationBinding durationBindingCodeHard;
    private DurationBinding durationBindingLadyJava;
    private DurationBinding durationBindingSweetHome;
    private void createMediaPlayers() {            
        mediaPlayers[MEDIA_PLAYER_CODE_HARD]  = new MediaPlayer(CODE_HARD_SONG);
        mediaPlayers[MEDIA_PLAYER_LADY_JAVA]  = new MediaPlayer(LADY_JAVA_SONG);
        mediaPlayers[MEDIA_PLAYER_SWEET_HOME] = new MediaPlayer(SWEET_HOME_SONG);
                
        durationBindingCodeHard = new DurationBinding(mediaPlayers[MEDIA_PLAYER_CODE_HARD].currentTimeProperty());
        mediaPlayers[MEDIA_PLAYER_CODE_HARD].currentTimeProperty().addListener(observable -> {
            if (currentMediaPlayer == MEDIA_PLAYER_CODE_HARD) {
                display.setRemaining(mediaPlayers[MEDIA_PLAYER_CODE_HARD].getMedia().getDuration().subtract(mediaPlayers[MEDIA_PLAYER_CODE_HARD].getCurrentTime()));
            }
        });        
        
        durationBindingLadyJava = new DurationBinding(mediaPlayers[MEDIA_PLAYER_LADY_JAVA].currentTimeProperty());
        mediaPlayers[MEDIA_PLAYER_LADY_JAVA].currentTimeProperty().addListener(observable -> {
            if (currentMediaPlayer == MEDIA_PLAYER_LADY_JAVA) {
                display.setRemaining(mediaPlayers[MEDIA_PLAYER_LADY_JAVA].getMedia().getDuration().subtract(mediaPlayers[MEDIA_PLAYER_LADY_JAVA].getCurrentTime()));
            }
        });
        
        durationBindingSweetHome = new DurationBinding(mediaPlayers[MEDIA_PLAYER_SWEET_HOME].currentTimeProperty());
        mediaPlayers[MEDIA_PLAYER_SWEET_HOME].currentTimeProperty().addListener(observable -> {
            if (currentMediaPlayer == MEDIA_PLAYER_SWEET_HOME) {
                display.setRemaining(mediaPlayers[MEDIA_PLAYER_SWEET_HOME].getMedia().getDuration().subtract(mediaPlayers[MEDIA_PLAYER_SWEET_HOME].getCurrentTime()));
            }
        });        
    }    

    void switchJogMode(boolean jog) {
        if (jog) {
            jogControl.valueProperty().unbind();
            jogControl.valueProperty().addListener(jogModeListener);
        } else {
            jogControl.valueProperty().removeListener(jogModeListener);
            jogControl.valueProperty().bind(durationBindingCodeHard);
        }
    }

    private JoystickControl createJoyStickControl() {
        JoystickControl joystickControl = new JoystickControl();
        joystickControl.setId("joystick");
        joystickControl.getStyleClass().add("joystick");
        joystickControl.setLayoutX(504);
        joystickControl.setLayoutY(111);
        return joystickControl;
    }

    private Button createTrackSearchButton(double width, double height, Node icon) {
        Button button = new Button();
        button.setPrefSize(width, height);
        Node back = createImage(Demo.class.getResource("search-metal-back.png").toString());

        StackPane pane = new StackPane(
                back, icon);
        button.setGraphic(pane);
        button.getStyleClass().add("track-search-button");

        return button;
    }

    private VinylControl createVinylControl() {
        VinylControl vinylControl = new VinylControl();
        vinylControl.setId("vinyl");
        vinylControl.getStyleClass().add("vinyl");
        vinylControl.setLayoutX(636);
        vinylControl.setLayoutY(200);
        return vinylControl;
    }

    private class ChangeListenerImpl implements ChangeListener<Number> {

        public ChangeListenerImpl() {
        }

        @Override public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number newValue) {
            if (newValue.doubleValue() > 0) {
                mediaPlayers[currentMediaPlayer].seek(Duration.seconds(newValue.doubleValue()));
            }
        }
    }

    
    
    class Delta {

        double x, y;
    }
}
