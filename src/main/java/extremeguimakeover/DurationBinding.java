/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package extremeguimakeover;


import javafx.beans.binding.DoubleBinding;
    import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 *
 * @author antonepple
 */
public class DurationBinding extends DoubleBinding {
    ReadOnlyObjectProperty<Duration> duration;
    public DurationBinding(ReadOnlyObjectProperty<Duration> duration) {
        super.bind(duration);
        this.duration = duration;
    }

    @Override
    protected double computeValue() {
        return duration.getValue().toSeconds();
    }

   

}
